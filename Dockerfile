from nginx:latest

copy index.html /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
